package proyecto.facci.recetasmanabitas.apprecetasmanabitas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class PrincipalActivity extends AppCompatActivity {
    ListView lista;

    String[][] datos = {
            {"Ceviche","Pescado","8","Ceviche de pescado: Esta receta de ceviche de pescado es de mi amiga Mafi, el pescado se cocina con jugo de limón y sal, luego se mezcla con cebolla, tomate, pimiento y cilantro.\n"+
                    "Ceviche de pulpo: Deliciosa receta de ceviche de pulpo preparado con pulpo, cebolla colorada, ajíes, jugo de limón, cilantro y aceite."},
            {"Caldo","Gallina","9","Para cocinar este rico plato, debes colocar las presas de la gallina en una olla con agua y dejar hervir durante 45 minutos a fuego medio y con la olla tapada. Asimismo, retirar la espuma que se vaya formando.\n" +
                    "Luego, echar el apio, kión, el cubito de gallina y papas enteras. Deje cocinar las presas y las verduras de 5 a 10 minutos con la olla destapada y añadir los fideos. Termine de cocinar todos los ingredientes 10 minutos más.\n" +
                    "Finalmente sirva este delicioso caldo acompañado de huevo cocido y papa, también puedes añadir cebolla china picada en cuadrados pequeños al gusto."},
            {"Arroz Marinero","Mariscos","8","Para preparar el arroz blanco\n" +
                    "En una olla mediana, caliente 2 cucharadas de aceite a fuego medio, agregue las 2 cucharadas de cebolla y ajo, y cocine hasta que estén suaves, alrededor de 3-5 minutos.\n" +
                    "Añada el arroz crudo, revuelva bien. Agregue el caldo de marisco o pescado, haga hervir a fuego alto hasta que el caldo se evapore. Reduzca a fuego bajo, tape y deje cocinar a fuego lento durante unos 15 minutos o hasta que el arroz esté tierno pero firme, guárdelo para después.\n" +
                    "Para preparar el arroz marinero\n" +
                    "En una sartén grande, caliente las 3 cucharadas de aceite a fuego medio. Agregue la cebolla picada y el ajo picado, revuelva frecuentemente y cocine hasta que las cebollas estén suaves, unos 5 minutos.\n" +
                    "Añada el comino, el achiote molido, sal, pimienta, la mitad del cilantro picado, y el pimiento picado. Cocine por 5 minutos, revolviendo frecuentemente.\n" +
                    "Agregue los mariscos, recomiendo agregar primero los mariscos que necesitan más tiempo para cocerse (los camarones grandes, las conchas o almejas, mejillones), seguido de los que requieren menos tiempo de cocción (los camarones pequeños, calamares, las vieiras o conchas de abanico). Revuelva bien y cocine durante unos 3 minutos.\n" +
                    "Agregue el arroz blanco cocido, mezcle bien y cocine hasta que los mariscos estén listos, aproximadamente unos 5 minutos.\n" +
                    "Agregue la mitad restante del cilantro picado, y rectifique la sal.\n" +
                    "Sirva acompañado con patacones o tostones, curtido de cebolla o ensalada, rebanadas de aguacate, limón, y ají criollo."},
            {"Seco","Gallina","7","Espolvoree las presas de pollo con el comino molido, el achiote molido, sal, y pimienta.\n" +
                    "Licue la cerveza y el jugo de naranjilla con los trozos de tomate, cebolla, dientes de ajos, pimientos, aji o chile si lo desea usar, cilantro, perejil, y orégano hasta obtener un puré o una salsa espesa.\n" +
                    "Caliente el aceite en una olla grande – en caso de usar las semillas de achiote, se las agrega al aceite a fuego bajo hasta que salga el color, y se quitan las semillas antes de agregar el pollo).\n" +
                    "Dore las presas de pollo (antes solía hacer un refrito con un poquito de cebolla picada y condimentos antes de dorar el pollo – si lo desea lo puede preparar así o seguir con la versión simplificada).\n" +
                    "Añada la mezcla licuada, haga hervir, y cocine a fuego lento hasta que la carne de pollo este muy suave, casi una hora (dependiendo de si es carne de pollo o carne de gallina).\n" +
                    "Si el pollo esta tierno pero la salsa aún no se ha espesado a su gusto, puede retirar las presas, y cocinar la salsa a fuego medio/alto hasta que se espese, unos 10 a 15 minutos. En este momento también la puede probar y ajustar la sal. Personalmente me gusta agregarle un poco de jugo de naranja recién exprimido para equilibrar cualquier sabor amargo de la cerveza. Cuando la salsa este al espesor de su preferencia, vuelva a poner las presas de pollo, y agregue el perejil/cilantro picado que guardo.\n" +
                    "Servir con arroz amarillo, plátanos maduros fritos, aguacate, papas, ensalada, etc."},
            {"Tonga","Pollo","7","Para armar la tonga tiene que tener primero limpias las hojas. Encima de cada hoja pone una taza de arroz amarillo cocinado y la aplana un poquito, luego pone una presa de gallina con su jugo, una buena cucharada de salsa de maní, un trozo de maduro frito y un poquito de culantro fresco picado. Luego cierra la hoja como si estuviera envolviendo una hayaca y la pone a baño maría para conservar la tonga caliente. (Aunque lo ideal es ponerla en un fogón de carbón, dijo Mariela).\n" +
                    "Debe servirla bien caliente acompañada de ají de la casa."},
            {"Fritada","Chancho","8","Sazone la carne de chancho con el comino molido, ajo machacado, sal y pimienta. Si tiene tiempo deje reposar en la refrigeradora por un par de horas.\n" +
                    "Ponga la carne de chancho, la cebolla, el chalote, los dientes de ajo enteros y el agua en una sartén grande y cocine hasta que ya casi no quede nada de agua.\n" +
                    "Añadía el jugo de naranja y cocine hasta se reduzca todo el liquido\n" +
                    "La carne se empieza a dorar, con una cuchara de palo revuelva la carne de vez en cuando para evitar que se queme. Cocine la carne hasta que todos los trozos estén dorados.\n" +
                    "En la misma sartén añada la yuca, el mote y los plátanos fritos, si desea puede cocinar los plátanos en la misma grasa de la fritada, llamada mapahuira, pero esto puede volverse complicado y resulta más fácil freírlos por separado.\n" +
                    "Sirva la fritada con la yuca, mote, plátanos fritos, curtido, aguacate y aji criollo."},
    };
    int [] datosImg = {R.drawable.ceviche,R.drawable.caldodegallina,R.drawable.arrozmarinero,R.drawable.secogallina,R.drawable.tonga,R.drawable.fritada};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        lista = (ListView)findViewById(R.id.listaViewRecetas);
        lista.setAdapter(new Adaptador(this,datos,datosImg));

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent visorDetalle = new Intent(view.getContext(),DetallesRecetas.class);
                visorDetalle.putExtra("TIT",datos[position][0]);
                visorDetalle.putExtra("DET",datos[position][3]);
                startActivity(visorDetalle);
            }
        });
    }
}
